Sample hello world taken from http://jetcracker.wordpress.com/2012/03/01/how-to-install-mpi-in-ubuntu/

# Setup
For Debian/Ubuntu:

	apt-get install make libcr-dev mpich2 mpich2-doc

For CentOS 6.x

	yum install mpich2-devel make

For CentOS 7.1

Warning: there is package mvapich2, but it crashes right away

	yum install mpich mpich-devel
# Compile on Ubuntu and CentOS6

	make

# Run on Ubuntu and CentOS 6
	make run

# Compile and run on CentOS 7

	PATH=/usr/lib64/mpich/bin:$PATH LDFLAGS="-L/usr/lib64/mpich/lib" make run
	
# Further information

See nice tutorials on https://computing.llnl.gov/tutorials/mpi/


